function btnRunClick(){

	var x1 = document.getElementById('x1').value;
	var x2 = document.getElementById('x2').value;

	if(!x1.trim() || !x2.trim()){
		alert("Empty fields!");
		return;
	}

	x1 = parseInt(x1);
	x2 = parseInt(x2);

	if(Number.isNaN(x1) || Number.isNaN(x2)){
		alert("Incorrect values. Fields x1&x2 require integers")
	} else {
		var resultDiv = document.getElementById('result');
		resultDiv.innerHTML = '';

		if(document.getElementById('sum').checked){
			var sum = 0;
			if((x2-x1) > 0){
				for(var i=x1; i<=x2; i++){
					sum+=i;
				}
			}
			else if((x2-x1) < 0){
				for(var i=x2; i<=x1; i++){
					sum+=i;
				}
			} 
			resultDiv.append(x1 + '+...+' + x2 + '=' + sum);
			return;
		}
		if(document.getElementById('prod').checked){
			var prod = x1;
			if((x2-x1) > 0){
				for(var i=x1; i<=x2; i++){
					prod*=i;
				}
			}
			else if((x2-x1) < 0){
				prod = x2;
				for(var i=x2; i<=x1; i++){
					prod*=i;
				}
			} 
			resultDiv.append(x1 + '*...*' + x2 + '=' + prod);
			return;
		} 
		if(document.getElementById('prime').checked){
			var lst = [];
			var prime = true;
			if((x2-x1) > 0){
				for(var i=x1; i<=x2; i++){
					for(var j=2; j<i; j++){
						if(i%j == 0){
							prime = false;
							break;
						}
					}
					if(i>1 && prime)
						lst.push(i);
					prime = true;
				}
			}
			else if((x2-x1) < 0){
				for(var i=x2; i<=x1; i++){
					for(var j=2; j<i; j++){
						if(i%j == 0){
							prime = false;
							break;
						}
					}
					if(i>1 && prime)
						lst.push(i);
					prime = true;
				}
			}
			resultDiv.append(lst);
		}else{
			alert("Choose method");
		}
	}
}

function btnDelClick(){
	document.getElementById('x1').value = '';
	document.getElementById('x2').value = '';
	document.getElementById('result').innerHTML = '';
}